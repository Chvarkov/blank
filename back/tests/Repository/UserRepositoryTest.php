<?php

namespace Tests\Repository;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserRepositoryTest extends KernelTestCase
{
    /** @var UserRepository */
    private $userRepository;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        /** @var EntityManager $em */
        $em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->userRepository = $em->getRepository(User::class);
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function testGetUserByLogin()
    {
        $user = $this->userRepository->getUserByUsername('edward');

        self::assertInstanceOf(User::class, $user);
        self::assertEquals('edward', $user->getUsername());
    }
}
