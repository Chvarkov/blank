# Blank project


## Backend

### Install dependencies

```
composer install
```

### Settings connection to database

Change parameter DATABASE_URL in .env (dev, test, prod) files. Set your connection string to database.

```
DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name
```

Note: for testing change parameter DATABASE_URL in phpunit.xml

### Execute migrations

```
php bin/console doctrine:migrations:status
php bin/console doctrine:migrations:migrate
```
### Load fixtures
```
php bin/console doctrine:fixtures:load
```

### Warm up cache
```
php bin/console cache:warmup
```

### Or run deploy file
```
sudo bash ./bin/deploy.sh
```

# Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


