import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RegisterModel } from '../models/register.model';
import * as jwt_decode from 'jwt-decode';
import {UserModel} from '../models/user.model';


@Injectable()
export class UserService {

  private apiHost = 'http://localhost:8000';

  private baseUri = '/api/user';

  private uri = {
    auth: '/auth',
    register: '/register'
  };

  public constructor(private http: HttpClient) {}

  public auth(credentials: {username: string, password: string}): Promise<any> {
    const body = JSON.stringify(credentials);

    return this.http.post(this.apiHost + this.baseUri + this.uri.auth, body, {
      headers: {
        'Content-type': 'application/json; charset=utf-8',
      },
    }).toPromise();
  }

  public register(register: RegisterModel): Promise<any> {
    return this.http.post(this.apiHost + this.baseUri + this.uri.register, JSON.stringify(register), {
      headers: {
        'Content-type': 'application/json; charset=utf-8',
      },
    }).toPromise();
  }

  public getUser(): UserModel|null {
    if (!this.isAuthenticated()) {
      return null;
    }

    try {
      return jwt_decode(localStorage.getItem('token_access'));
    } catch (error) {
      return null;
    }
  }

  public isAuthenticated(): boolean {
    return null !== localStorage.getItem('token_access');
  }

  public logout(): void {
    localStorage.removeItem('token_access');
  }
}
