<?php

namespace Tests\Repository;

use App\Entity\ReleaseNote;
use App\Repository\ReleaseNoteRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ReleaseNoteRepositoryTest extends KernelTestCase
{
    /** @var ReleaseNoteRepository */
    private $releaseNoteRepository;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        /** @var EntityManager $em */
        $em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->releaseNoteRepository = $em->getRepository(ReleaseNote::class);
    }

    public function testGetList()
    {
        $list = $this->releaseNoteRepository->getList();

        self::assertTrue(\count($list) > 0);
        self::assertArrayHasKey(0, $list);

        $releaseNote = $list[0];
        self::assertInstanceOf(ReleaseNote::class, $releaseNote);
    }
}
