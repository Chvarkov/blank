export class ResponseModel<T> {
  public code: number;
  public content: Array<T>;

  public isSuccess(): boolean {
    return this.code >= 200 && this.code < 300;
  }
}
