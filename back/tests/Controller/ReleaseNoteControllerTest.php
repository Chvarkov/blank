<?php

namespace Tests;

use App\Tests\TestCase\ApiTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ReleaseNoteControllerTest extends ApiTestCase
{
    public function setUp()
    {
        parent::setUp();

        self::authAsTestUser();
    }

    public function testGetList()
    {
        $this->setBody([
           'page' => 2,
           'limit' => 3,
        ]);

        $response = $this->makeRequest($this->getUrl('api_release_note_list'));

        self::assertStatusCode($response, Response::HTTP_OK);

        $content = \json_decode($response->getContent(), true);

        self::assertIsArray($content);
        self::assertArrayHasKey('content', $content);
        self::assertCount(3, $content['content']);

        $releaseNote = \array_shift($content['content']);
        self::assertArrayHasKey('title', $releaseNote);
        self::assertArrayHasKey('description', $releaseNote);
        self::assertArrayHasKey('dateAdded', $releaseNote);
    }

    public function testGetListInvalidParameters()
    {
        $this->setBody([
            'page' => -1,
            'limit' => -1,
        ]);

        $response = $this->makeRequest($this->getUrl('api_release_note_list'));

        self::assertStatusCode($response, Response::HTTP_BAD_REQUEST);
    }

    public function testAdd()
    {
        $this->setBody([
            'title' => 'Test',
            'description' => 'Successful',
        ]);

        $response = $this->makeRequest($this->getUrl('api_release_note_add'), Request::METHOD_POST);

        self::assertStatusCode($response, Response::HTTP_NO_CONTENT);
    }

    public function testAddInvalid()
    {
        $this->setBody([
            'title' => '#',
            'description' => null,
        ]);

        $response = $this->makeRequest($this->getUrl('api_release_note_add'), Request::METHOD_POST);

        self::assertStatusCode($response, Response::HTTP_BAD_REQUEST);
    }

    public function testEdit()
    {
        $id = 11;

        $this->setBody([
            'title' => 'Edit',
            'description' => 'Successful',
        ]);

        $response = $this->makeRequest($this->getUrl('api_release_note_edit', ['id' => $id]), Request::METHOD_PUT);

        self::assertStatusCode($response, Response::HTTP_NO_CONTENT);
    }

    public function testEditInvalid()
    {
        $id = 11;

        $this->setContent([
            'title' => '#',
            'description' => null,
        ]);

        $response = $this->makeRequest($this->getUrl('api_release_note_edit', ['id' => $id]), Request::METHOD_PUT);

        self::assertStatusCode($response, Response::HTTP_BAD_REQUEST);
    }

    public function testEditNotFound()
    {
        $id = 99999999;

        $this->setContent([
            'title' => 'Test',
            'description' => 'Test',
        ]);

        $response = $this->makeRequest($this->getUrl('api_release_note_edit', ['id' => $id]), Request::METHOD_PUT);

        self::assertStatusCode($response, Response::HTTP_NOT_FOUND);
    }
}
