import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public form = new FormGroup({
    first: new FormControl('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(32),
      Validators.pattern('^[A-z]*$')
    ]),
    last: new FormControl('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(32),
      Validators.pattern('^[A-z]*$')
    ]),
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(32),
      Validators.pattern('^[A-z0-9_]*$')
    ]),
    email: new FormControl('', [
      Validators.required,
      Validators.email
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(32)
    ]),
    repeatPassword: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(32)
    ])
  },  (form: FormGroup) => {
    return form.controls.password.value === form.controls.repeatPassword.value ? null :  { equals: true };
  });

  get first(): AbstractControl {
    return this.form.get('first');
  }

  get last(): AbstractControl {
    return this.form.get('last');
  }

  get username(): AbstractControl {
    return this.form.get('username');
  }

  get email(): AbstractControl {
    return this.form.get('email');
  }

  get password(): AbstractControl {
    return this.form.get('password');
  }

  get repeatPassword(): AbstractControl {
    return this.form.get('repeatPassword');
  }

  public constructor(private userService: UserService, private router: Router) {}

  public ngOnInit() {}

  public onSubmit($event: MouseEvent) {
    $event.preventDefault();

    const promise: Promise<any> = this.userService.register(this.form.value);

    promise.then((data: { content: string}) => {
      if (data.content) {
        console.log(data.content);
        this.router.navigateByUrl('/login');
      }
    }).catch( e => {
        console.log(e.message);
      }
    );
  }
}
