<?php

namespace Tests;

use App\Tests\TestCase\ApiTestCase;
use Symfony\Component\HttpFoundation\Response;

class UserControllerTest extends ApiTestCase
{
    public function testRegister()
    {
        $userData = [
            'username' => \uniqid('test_'),
            'password' => '123456',
            'repeatPassword' => '123456',
            'email' => \uniqid().'@fakemail.com',
            'first' => 'Firstname',
            'last' => 'Lastname',
        ];

        $this->setBody($userData);

        $response = $this->makeRequest($this->getUrl('api_user_register'), 'POST');

        self::assertStatusCode($response, Response::HTTP_OK);
    }

    public function testRegisterDiffPasswords()
    {
        $userData = [
            'username' => \uniqid('test_'),
            'password' => \uniqid(),
            'repeatPassword' => \uniqid(),
            'email' => \uniqid().'@fakemail.com',
            'first' => 'Firstname',
            'last' => 'Lastname',
        ];

        $this->setBody($userData);

        $response = $this->makeRequest($this->getUrl('api_user_register'), 'POST');

        self::assertStatusCode($response, Response::HTTP_BAD_REQUEST);
    }

    public function testRegisterEmailAlreadyExists()
    {
        $userData = [
            'username' => \uniqid('test_'),
            'password' => '123456',
            'repeatPassword' => '123456',
            'email' => 'willy.thompson@fake-mail.com',
            'first' => 'Firstname',
            'last' => 'Lastname',
        ];

        $this->setBody($userData);

        $response = $this->makeRequest($this->getUrl('api_user_register'), 'POST');

        self::assertStatusCode($response, Response::HTTP_BAD_REQUEST);
    }

    public function testRegisterUsernameAlreadyExists()
    {
        $userData = [
            'username' => 'willy',
            'password' => '123456',
            'repeatPassword' => '123456',
            'email' => \uniqid().'@fakemail.com',
            'first' => 'Firstname',
            'last' => 'Lastname',
        ];

        $this->setBody($userData);

        $response = $this->makeRequest($this->getUrl('api_user_register'), 'POST');

        self::assertStatusCode($response, Response::HTTP_BAD_REQUEST);
    }

    public function testRegisterInvalidData()
    {
        $userData = [
            'username' => 'USER_#%^^##',
            'password' => 'ff',
            'repeatPassword' => 'ff',
            'email' => 'i not email',
            'first' => 'F',
            'last' => 'L',
        ];

        $this->setBody($userData);

        $response = $this->makeRequest($this->getUrl('api_user_register'), 'POST');

        self::assertStatusCode($response, Response::HTTP_BAD_REQUEST);

        $content = \json_decode($response->getContent(), true);
        self::assertIsArray($content);
        self::assertArrayHasKey('errors', $content);
        self::assertIsString($content['errors']);

        $errors = \explode(', ', $content['errors']);
        self::assertEquals(\count($userData), \count($errors));
    }

    public function testEdit()
    {
        $this->setContent([
            'username' => 'edward',
            'password' => 'edward',
        ]);

        $response = $this->makeRequest($this->getUrl('api_user_auth'), 'POST');

        self::assertStatusCode($response, Response::HTTP_OK);

        $content = \json_decode($response->getContent(), true);

        $token = $content['token'];

        $this->setToken($token);

        $userData = [
            'username' => 'edward',
            'password' => 'edward',
            'email' => 'eddy@fakemail.com',
            'first' => 'Eddy',
            'last' => 'Todd',
        ];

        $this->setBody($userData);

        $response = $this->makeRequest($this->getUrl('api_user_edit'), 'PUT');

        self::assertStatusCode($response, Response::HTTP_NO_CONTENT);
    }

    public function testEditInvalidData()
    {
        $this->setContent([
            'username' => 'edward',
            'password' => 'edward',
        ]);

        $response = $this->makeRequest($this->getUrl('api_user_auth'), 'POST');

        self::assertStatusCode($response, Response::HTTP_OK);

        $content = \json_decode($response->getContent(), true);

        $token = $content['token'];

        $this->setToken($token);

        $userData = [
            'username' => 'USER_#%^^##',
            'password' => 'ff',
            'email' => 'i not email',
            'first' => 'F',
            'last' => 'L',
        ];

        $this->setBody($userData);

        $response = $this->makeRequest($this->getUrl('api_user_edit'), 'PUT');

        self::assertStatusCode($response, Response::HTTP_BAD_REQUEST);
    }
}
