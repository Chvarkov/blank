import { Component, OnInit } from '@angular/core';
import { ReleaseNoteModel } from '../../models/releaseNote.model';
import { ReleaseNoteService } from '../../services/releaseNote.service';
import { UserService } from '../../services/user.service';
import {ResponseModel} from '../../models/response.model';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  public releaseNotes: ReleaseNoteModel[];

  public constructor(private releaseNoteService: ReleaseNoteService, private userService: UserService) { }

  public ngOnInit() {

    if (!this.userService.isAuthenticated()) {
      return;
    }

    this.releaseNoteService.getList().then((data: ResponseModel<ReleaseNoteModel>) => {
      this.releaseNotes = data.content;
    }).catch( e => {
        console.log(e.message);
      }
    );
  }
}
