<?php

namespace App\Component\Exception;

use FOS\RestBundle\View\View;

/**
 * Interface ApiHttpExceptionInterface.
 */
interface ApiHttpExceptionInterface
{
    /**
     * Get view.
     *
     * @return View
     */
    public function getView(): View;
}
