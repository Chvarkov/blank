<?php

namespace App\DataFixtures\Fixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Yaml\Yaml;

class UserFixture extends Fixture
{
    private $fileName = 'Users.yml';

    public function load(ObjectManager $manager)
    {
        $users = Yaml::parseFile('src/DataFixtures/data/'.$this->fileName);

        foreach ($users as $user) {
            $manager->persist(
                new User(
                    $user['username'],
                    $user['email'],
                    $user['passwordHash'],
                    $user['first'],
                    $user['last']
                )
            );
        }
        $manager->flush();
    }
}
