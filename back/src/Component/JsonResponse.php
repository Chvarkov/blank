<?php

namespace App\Component;

use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class JsonResponse.
 */
class JsonResponse
{
    /** @var int */
    private $code;

    /** @var mixed */
    private $content;

    /** @var int */
    private $total;

    /**
     * JsonResponse constructor.
     *
     * @param mixed $content
     * @param int   $code
     */
    public function __construct($content = null, $code = 200)
    {
        $this->content = $content;
        $this->code = $code;
    }

    /**
     * @param int $total
     *
     * @return JsonResponse
     */
    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @param int $code
     *
     * @return JsonResponse
     */
    public function setCode(int $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return View
     */
    public function getView(): View
    {
        return (new View())
            ->setData($this->getData())
            ->setStatusCode($this->code);
    }

    /**
     * @return bool
     */
    protected function isSuccessStatusCode(): bool
    {
        return $this->code >= 200 && $this->code < 300;
    }

    /**
     * @return array
     */
    protected function getData(): array
    {
        $dataKey = 'content';

        if (!$this->isSuccessStatusCode()) {
            $dataKey = 'errors';

            if (empty($this->content)) {
                $this->content = Response::$statusTexts[$this->code];
            }
        }

        $data = [
            'code' => $this->code,
            $dataKey => $this->content,
        ];

        if (\is_int($this->total)) {
            $data['total'] = $this->total;
        }

        return $data;
    }
}
