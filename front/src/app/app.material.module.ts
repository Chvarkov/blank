import { NgModule } from '@angular/core';
import { AvatarModule } from 'ngx-avatar';
import {
  MatButtonModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatSidenavModule,
  MatCheckboxModule,
  MatListModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatInputModule,
  MatSelectModule,
  MatDatepickerModule,
  MatStepperModule,
  MatSnackBarModule,
  MatDialogModule,
  MatGridListModule,
  MatAutocompleteModule,
  MatChipsModule,
  MatTooltipModule,
  MatRadioModule
} from '@angular/material';

export const matModules = [
  MatButtonModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatSidenavModule,
  MatCheckboxModule,
  MatListModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatSortModule,
  MatInputModule,
  MatSelectModule,
  MatDatepickerModule,
  MatStepperModule,
  MatSnackBarModule,
  MatDialogModule,
  MatGridListModule,
  MatAutocompleteModule,
  MatChipsModule,
  MatTooltipModule,
  MatRadioModule,
  AvatarModule,
];

@NgModule({
  imports: matModules,
  exports: matModules,
})
export class AppMaterialModule {
}
