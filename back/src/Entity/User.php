<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class entity of User.
 *
 * @ORM\Table(name="Users")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /** Default photo of User if photo is not uploaded */
    public const DEFAULT_PHOTO = '/public/img/photo.png';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer", length=255, nullable=false)
     *
     * @var int
     */
    protected $id;

    /**
     * @Assert\NotNull(message="Username cannot be null")
     * @Assert\NotBlank(message="Username cannot be blank")
     * @Assert\Length(min="3",  minMessage="Username cannot be less than 3 characters",
     *                max="32", maxMessage="Username cannot be longer than 32 characters"
     * )
     * @Assert\Regex(pattern="/^[A-z0-9_]*$/",
     *               message="Login can be contains only letters numbers and underscore."
     * )
     *
     * @ORM\Column(name="username", type="string", length=32, nullable=false)
     *
     * @var string
     */
    protected $username;

    /**
     * @Assert\NotNull(message="Email cannot be null")
     * @Assert\NotBlank(message="Email cannot be blank")
     * @Assert\Email(message="Invalid email")
     *
     *  @ORM\Column(name="email", type="string", length=32, nullable=false)
     *
     * @var string
     */
    protected $email;

    /**
     * @Assert\NotNull(message="Password cannot be null")
     * @Assert\NotBlank(message="Password cannot be blank")
     * @Assert\Length(min="6",  minMessage="Password cannot be less than 6 characters",
     *                max="32", maxMessage="Password cannot be longer than 32 characters"
     * )
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     *
     * @var string
     */
    protected $password;

    /**
     * @Assert\NotNull(message="First cannot be null")
     * @Assert\NotBlank(message="First name cannot be blank")
     * @Assert\Length(min="2",  minMessage="First name cannot be less than 2 characters",
     *                max="32", maxMessage="First name cannot be longer than 32 characters"
     * )
     *
     * @ORM\Column(name="first", type="string", length=32, nullable=false)
     *
     * @var string
     */
    protected $first;

    /**
     * @Assert\NotNull(message="Last cannot be null")
     * @Assert\NotBlank(message="Last name cannot be blank")
     * @Assert\Length(min="2",  minMessage="Last name cannot be less than 2 characters",
     *                max="32", maxMessage="Last name cannot be longer than 32 characters"
     * )
     *
     * @ORM\Column(name="last", type="string", length=32, nullable=false)
     *
     * @var string
     */
    protected $last;

    /**
     * @ORM\Column(name="photo", type="string", length=255, nullable=true)
     *
     * @var string
     */
    protected $photo;

    /**
     * @ORM\Column(name="dateAdded", type="datetime", length=255, nullable=false)
     *
     * @var \DateTime
     */
    protected $dateAdded;

    /**
     * User constructor.
     *
     * @param string         $username
     * @param string         $email
     * @param string         $password
     * @param string         $first
     * @param string         $last
     * @param string|null    $photo
     * @param \DateTime|null $dateAdded
     */
    public function __construct(
        string $username,
        string $email,
        string $password,
        string $first,
        string $last,
        ?string $photo = null,
        ?\DateTime $dateAdded = null
    ) {
        $this->username = $username;
        $this->email = $email;
        $this->password = $password;
        $this->first = $first;
        $this->last = $last;
        $this->photo = $photo ?? self::DEFAULT_PHOTO;
        $this->dateAdded = $dateAdded ?? new \DateTime();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     *
     * @return User
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return User
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return User
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirst(): string
    {
        return $this->first;
    }

    /**
     * @param string $first
     *
     * @return User
     */
    public function setFirst(string $first): self
    {
        $this->first = $first;

        return $this;
    }

    /**
     * @return string
     */
    public function getLast(): string
    {
        return $this->last;
    }

    /**
     * @param string $last
     *
     * @return User
     */
    public function setLast(string $last): self
    {
        $this->last = $last;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhoto(): string
    {
        return $this->photo;
    }

    /**
     * @param string $photo
     *
     * @return User
     */
    public function setPhoto(string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateAdded(): \DateTime
    {
        return $this->dateAdded;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return ['ROLE_API'];
    }

    /**
     * @return string|null
     */
    public function getSalt(): ?string
    {
        return $this->id.$this->dateAdded->getTimestamp();
    }

    public function eraseCredentials()
    {
    }
}
