<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * Class UserRepository entity of @see User
 * Table in database Users.
 */
class UserRepository extends EntityRepository
{
    /**
     * @param string $username
     *
     * @return User|null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getUserByUsername(string $username): ?User
    {
        return $this->createQueryBuilder('user')
            ->where('user.username = :username')
            ->setParameter('username', $username)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
