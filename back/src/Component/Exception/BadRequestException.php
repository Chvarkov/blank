<?php

namespace App\Component\Exception;

use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;

/**
 * Class BadRequestException.
 */
class BadRequestException extends ApiHttpException
{
    protected $code = 400;

    /**
     * BadRequestException constructor.
     *
     * @param string|FormInterface|null $content
     */
    public function __construct($content = null)
    {
        if ($content instanceof FormInterface) {
            $errorMessages = [];

            /** @var FormError $error */
            foreach ($content->getErrors(true) as $error) {
                $errorMessages[] = $error->getMessage();
            }

            $content = implode(', ', $errorMessages);
        }

        if (!\is_string($content) || 0 === mb_strlen($content)) {
            $content = null;
        }

        parent::__construct($content);
    }
}
