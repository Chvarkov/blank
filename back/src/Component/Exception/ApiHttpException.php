<?php

namespace App\Component\Exception;

use App\Component\JsonResponse;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;

class ApiHttpException extends \Exception implements ApiHttpExceptionInterface
{
    public function __construct(?string $message = null)
    {
        parent::__construct($message ?? Response::$statusTexts[$this->code], $this->code);
    }

    /**
     * {@inheritdoc}
     */
    public function getView(): View
    {
        return (new JsonResponse($this->message, $this->code))->getView();
    }
}
