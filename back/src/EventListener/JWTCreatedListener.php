<?php

namespace App\EventListener;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;

class JWTCreatedListener
{
    public function onJwtCreated(JWTCreatedEvent $event)
    {
        /** @var User $user */
        $user = $event->getUser();

        $event->setData([
            'id' => $user->getId(),
            'username' => $user->getUsername(),
            'first' => $user->getFirst(),
            'last' => $user->getLast(),
            'email' => $user->getEmail(),
            'photo' => $user->getPhoto(),
            'roles' => $user->getRoles(),
        ]);
    }
}
