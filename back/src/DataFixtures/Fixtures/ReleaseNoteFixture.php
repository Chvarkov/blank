<?php

namespace App\DataFixtures\Fixtures;

use App\Entity\ReleaseNote;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Yaml\Yaml;

class ReleaseNoteFixture extends Fixture
{
    private $fileName = 'ReleaseNotes.yml';

    public function load(ObjectManager $manager)
    {
        $notes = Yaml::parseFile('src/DataFixtures/data/'.$this->fileName);

        foreach ($notes as $note) {
            $dateAdded = \DateTime::createFromFormat('U', $note['dateAdded']);

            $manager->persist(
                new ReleaseNote(
                    $note['title'],
                    $note['description'],
                    $dateAdded instanceof \DateTime ? $dateAdded : null
                )
            );
        }
        $manager->flush();
    }
}
