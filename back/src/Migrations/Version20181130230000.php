<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add table Users.
 */
class Version20181130230000 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("create table {$schema->getName()}.Users
        (
            id           int auto_increment primary key,
            username     varchar(32)  not null,
            email        varchar(255) not null,
            password     varchar(255) not null,
            first        varchar(32)  not null,
            last         varchar(32)  not null,
            photo        varchar(255) null,
            dateAdded    datetime     not null,
            apiToken     varchar(255) null,
            constraint Users_email_uIndex
            unique (email),
            constraint Users_username_uIndex
            unique (username)
        ) engine=InnoDB default charset=utf8 collate=utf8_unicode_ci");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("drop table {$schema->getName()}.Users");
    }
}
