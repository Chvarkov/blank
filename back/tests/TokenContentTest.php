<?php

namespace Tests;

use App\Tests\TestCase\ApiTestCase;

class TokenContentTest extends ApiTestCase
{
    public const EXPECTED_TYP = 'JWT';
    public const EXPECTED_ALG = 'RS256';

    public function setUp()
    {
        parent::setUp();
        $this->authAsTestUser();
    }

    public function testTokenContent()
    {
        $data = $this->decodeJwtToken($this->token);

        // jwt header
        self::assertArrayHasKey('header', $data);
        $header = $data['header'];
        self::assertIsArray($header);
        self::assertArrayHasKey('typ', $header);
        self::assertArrayHasKey('alg', $header);
        self::assertEquals(self::EXPECTED_TYP, $header['typ']);
        self::assertEquals(self::EXPECTED_ALG, $header['alg']);

        // jwt payload
        self::assertArrayHasKey('payload', $data);
        $payload = $data['payload'];
        self::assertArrayHasKey('iat', $payload);
        self::assertArrayHasKey('exp', $payload);
        self::assertArrayHasKey('id', $payload);
        self::assertArrayHasKey('username', $payload);
        self::assertArrayHasKey('email', $payload);
        self::assertArrayHasKey('first', $payload);
        self::assertArrayHasKey('last', $payload);
        self::assertArrayHasKey('photo', $payload);
        self::assertArrayHasKey('roles', $payload);

        self::assertIsInt($payload['iat']);
        self::assertIsInt($payload['exp']);
        self::assertIsInt($payload['id']);
        self::assertIsString($payload['username']);
        self::assertIsString($payload['email']);
        self::assertIsString($payload['first']);
        self::assertIsString($payload['last']);
        self::assertIsString($payload['photo']);
        self::assertIsArray($payload['roles']);
    }

    /**
     * @param string $token
     *
     * @return array
     */
    public function decodeJwtToken(string  $token): array
    {
        $tokenParts = \explode('.', $token);
        $tokenHeader = \base64_decode($tokenParts[0]);
        $tokenPayload = \base64_decode($tokenParts[1]);

        return [
            'header' => \json_decode($tokenHeader, true),
            'payload' => \json_decode($tokenPayload, true),
        ];
    }
}
