import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn } from '@angular/forms';
import { Directive, Input } from '@angular/core';


@Directive({
    selector: '[appEqualsValidator]',
    providers: [{ provide: NG_VALIDATORS, useExisting: EqualsValidator, multi: true }]
})
export class EqualsValidator implements Validator {

  @Input('equals')
  private value: string;

  public validate(control: AbstractControl): { [key: string]: any } {

    if (this.value) {
      return this.equals(this.value)(control);
    }

    return null;
  }

  public equals(value: string): ValidatorFn {
    this.value = value;

    return (control: AbstractControl): { [key: string]: any } => {
      if (control.value === this.value) {
        return null;
      }

      return {
        equals: {value: control.value}
      };
    };
  }
}
