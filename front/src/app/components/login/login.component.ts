import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  form = new FormGroup({
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(32),
      Validators.pattern('^[A-z0-9_]*$')
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(32)
    ])
  });

  get username(): AbstractControl {
    return this.form.get('username');
  }

  get password(): AbstractControl {
    return this.form.get('password');
  }

  public constructor(private userService: UserService, private router: Router) {}

  public ngOnInit(): void {}

  public onSubmit($event: MouseEvent): void {
    $event.preventDefault();

    this.userService.auth(this.form.value)
      .then((data: { token: string }) => {
        if (data.token) {
          localStorage.setItem('token_access', data.token);
          this.router.navigateByUrl('/');
        }
      }).catch(e => {
        console.log(e.message);
      }
    );
  }
}
