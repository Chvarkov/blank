import { Component } from '@angular/core';
import { UserService } from './services/user.service';
import { Router } from '@angular/router';
import { UserModel } from './models/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public title = 'Blank application';

  public drawerMode = 'over';

  public user: UserModel|null;

  public constructor(protected userService: UserService, private router: Router) {
    this.user = this.userService.getUser();
    if (!this.user) {
      this.userService.logout();
      this.router.navigateByUrl('/login');
    }
  }

  public logout() {
    this.userService.logout();
    this.router.navigateByUrl('/');
  }
}
