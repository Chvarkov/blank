<?php

namespace App\Form\Type;

use App\Component\Pagination;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PaginationType.
 */
class PaginationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('page', IntegerType::class, [
                'required' => false,
                'empty_data' => (string) Pagination::DEFAULT_PAGE,
            ])
            ->add('limit', IntegerType::class, [
                'required' => false,
                'empty_data' => (string) Pagination::DEFAULT_LIMIT,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => Pagination::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
