<?php

namespace Tests;

use App\Tests\TestCase\ApiTestCase;
use Symfony\Component\HttpFoundation\Response;

class SecurityTest extends ApiTestCase
{
    public function testUnauthorized()
    {
        $response = $this->makeRequest($this->getUrl('api_release_note_list'));

        self::assertStatusCode($response, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @return string
     */
    public function testAuthorization(): string
    {
        $this->setContent([
            'username' => 'edward',
            'password' => 'edward',
        ]);

        $response = $this->makeRequest($this->getUrl('api_user_auth'), 'POST');

        self::assertStatusCode($response, Response::HTTP_OK);

        $content = \json_decode($response->getContent(), true);
        self::assertIsArray($content);
        self::assertArrayHasKey('token', $content);
        self::assertIsStringNotEmpty($content['token']);

        return $content['token'];
    }

    /**
     * @depends testAuthorization
     *
     * @param string $token
     */
    public function testAuthorized(string $token)
    {
        $this->setToken($token);
        $response = $this->makeRequest($this->getUrl('api_release_note_list'));

        self::assertStatusCode($response, Response::HTTP_OK);
    }
}
