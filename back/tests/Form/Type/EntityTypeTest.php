<?php

namespace App\Tests\Form\Type;

use App\Form\Type\EntityFormType;
use App\Tests\Form\DTO\TestChildrenClass;
use App\Tests\Form\DTO\TestClass;
use App\Tests\Form\Type\TestType\TestClassFormType;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;

class EntityType extends KernelTestCase
{
    public function setUp()
    {
        $this->bootKernel();
        $this->createKernel();
    }

    /**
     * @return FormInterface
     */
    public function testMapDataToForms(): FormInterface
    {
        /** @var FormFactory $formFactory */
        $formFactory = self::$container->get('form.factory');
        $form = $formFactory->create(TestClassFormType::class, null);
        $testClass = $this->getTestClass();
        $formType = new TestClassFormType();

        $formType->mapDataToForms($testClass, $form);

        $returnForm = $form;

        $form = iterator_to_array($form);

        self::assertArrayHasKey('bool', $form);
        self::assertArrayHasKey('int', $form);
        self::assertArrayHasKey('float', $form);
        self::assertArrayHasKey('string', $form);
        self::assertArrayHasKey('array', $form);
        self::assertArrayHasKey('nullable', $form);
        self::assertArrayHasKey('datetime', $form);
        self::assertArrayHasKey('childClass', $form);

        /* @var FormInterface $form */
        self::assertEquals($testClass->isBool(), $form['bool']->getNormData());
        self::assertEquals($testClass->getInt(), $form['int']->getNormData());
        self::assertEquals($testClass->getFloat(), $form['float']->getNormData());
        self::assertEquals($testClass->getString(), $form['string']->getNormData());
        self::assertEquals($testClass->getArray(), $form['array']->getNormData());
        self::assertEquals(null, $form['nullable']->getNormData());
        self::assertInstanceOf(\DateTime::class, $form['datetime']->getNormData());
        self::assertInstanceOf(TestChildrenClass::class, $form['childClass']->getNormData());

        $expectedChild = $testClass->getChildClass();
        /** @var TestChildrenClass $actualChild */
        $actualChild = $form['childClass']->getNormData();

        self::assertEquals($expectedChild->isBool(), $actualChild->isBool());
        self::assertEquals($expectedChild->getInt(), $actualChild->getInt());
        self::assertEquals($expectedChild->getFloat(), $actualChild->getFloat());
        self::assertEquals($expectedChild->getString(), $actualChild->getString());
        self::assertEquals($expectedChild->getArray(), $actualChild->getArray());
        self::assertEquals($expectedChild->getNullable(), $actualChild->getNullable());
        self::assertEquals($expectedChild->getDatetime(), $actualChild->getDatetime());

        return $returnForm;
    }

    public function testMapDataToFormsUnexpectedType()
    {
        /** @var FormFactory $formFactory */
        $formFactory = self::$container->get('form.factory');
        $form = $formFactory->create(TestClassFormType::class);
        $formType = new TestClassFormType();

        $this->expectException(UnexpectedTypeException::class);

        $formType->mapDataToForms([], $form);
    }

    /**
     * @depends testMapDataToForms
     *
     * @param FormInterface $form
     *
     * @throws \ReflectionException
     */
    public function testMapFormsToData(FormInterface $form)
    {
        $formType = new TestClassFormType();

        /** @var TestClass|null $data */
        $data = null;

        $form->remove('defaultValue');
        $form->remove('nullable');

        $formType->mapFormsToData($form, $data);

        self::assertInstanceOf(TestClass::class, $data);

        self::assertEquals($form['bool']->getNormData(), $data->isBool());
        self::assertEquals($form['int']->getNormData(), $data->getInt());
        self::assertEquals($form['float']->getNormData(), $data->getFloat());
        self::assertEquals($form['string']->getNormData(), $data->getString());
        self::assertEquals($form['array']->getNormData(), $data->getArray());
        self::assertEquals(null, $data->getNullable());
        self::assertEquals($form['datetime']->getNormData(), $data->getDatetime());
        self::assertEquals('Test', $data->getDefaultValue());
        self::assertEquals($form['childClass']->getNormData(), $data->getChildClass());

        $expectedChild = $data->getChildClass();
        /** @var TestChildrenClass $actualChild */
        $actualChild = $form['childClass']->getNormData();

        self::assertEquals($expectedChild->isBool(), $actualChild->isBool());
        self::assertEquals($expectedChild->getInt(), $actualChild->getInt());
        self::assertEquals($expectedChild->getFloat(), $actualChild->getFloat());
        self::assertEquals($expectedChild->getString(), $actualChild->getString());
        self::assertEquals($expectedChild->getArray(), $actualChild->getArray());
        self::assertEquals($expectedChild->getNullable(), $actualChild->getNullable());
        self::assertEquals($expectedChild->getDatetime(), $actualChild->getDatetime());
    }

    /**
     * @throws \ReflectionException
     */
    public function testMapFormsToDataIsFormIsNotForm()
    {
        $formType = new TestClassFormType();
        $data = null;

        $formType->mapFormsToData('form', $data);

        self::assertNull($data);
    }

    /**
     * @throws \ReflectionException
     */
    public function testMapFormsToDataEmptyDataClass()
    {
        /** @var FormFactory $formFactory */
        $formFactory = self::$container->get('form.factory');
        $form = $formFactory->create(TestClassFormType::class, null, [
            'data_class' => null,
        ]);

        $formType = new TestClassFormType();
        $data = null;

        $formType->mapFormsToData($form, $data);

        self::assertNull($data);
    }

    public function testEmptyDataNullDataClass()
    {
        /** @var FormFactory $formFactory */
        $formFactory = self::$container->get('form.factory');
        $form = $formFactory->create(TestClassFormType::class, null, [
            'data_class' => null,
        ]);

        $emptyData = $form->getConfig()->getEmptyData();
        self::assertInstanceOf(\Closure::class, $emptyData);

        $emptyValue = $emptyData($form);
        self::assertIsArray($emptyValue);
        self::assertCount(0, $emptyValue);
    }

    public function testEmptyDataForEmptyForm()
    {
        /** @var FormFactory $formFactory */
        $formFactory = self::$container->get('form.factory');
        $form = $formFactory->create(EntityFormType::class, null, [
            'data_class' => TestClass::class,
            'required' => false,
        ]);

        $emptyData = $form->getConfig()->getEmptyData();
        self::assertInstanceOf(\Closure::class, $emptyData);

        $emptyValue = $emptyData($form);
        self::assertNull($emptyValue);
    }

    public function testEmptyData()
    {
        /** @var FormFactory $formFactory */
        $formFactory = self::$container->get('form.factory');
        $form = $formFactory->create(TestClassFormType::class, null);

        $emptyData = $form->getConfig()->getEmptyData();
        self::assertInstanceOf(\Closure::class, $emptyData);

        $emptyValue = $emptyData($form);
        self::assertInstanceOf(TestClass::class, $emptyValue);
    }

    public function testPreSubmitInvalidData()
    {
        /** @var FormFactory $formFactory */
        $formFactory = self::$container->get('form.factory');
        $form = $formFactory->create(TestClassFormType::class, null, [
            'data_class' => null,
        ]);

        $form->submit(null);

        $data = $form->getData();
        self::assertIsArray($data);
        self::assertCount(0, $data);
    }

    /**
     * @return TestClass
     */
    private function getTestClass(): TestClass
    {
        return new TestClass(
            true,
            2,
            3.14,
            'test',
            [1, 2, 3],
            null,
            new \DateTime('2018-05-19 19:40'),
            [],
            null,
            new TestChildrenClass(
                true,
                2,
                3.14,
                'test',
                [1, 2, 3],
                null,
                new \DateTime('2018-05-19 19:40')
            )
        );
    }
}
