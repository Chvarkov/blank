import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainComponent } from './main.component';
import {AppMaterialModule} from '../../app.material.module';
import {UserService} from '../../services/user.service';
import {ReleaseNoteService} from '../../services/releaseNote.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';

describe('MainComponent', () => {
  let component: MainComponent;
  let fixture: ComponentFixture<MainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainComponent ],
      providers: [ UserService, ReleaseNoteService, HttpClient ],
      imports: [
        HttpClientModule,
        AppMaterialModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
