<?php

namespace Tests\Entity;

use App\Entity\ReleaseNote;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class ReleaseNoteTest extends TestCase
{
    public function testEntity()
    {
        $data = [
            'title' => 'Title',
            'description' => 'Description',
            'dateAdded' => new \DateTime(),
        ];

        $releaseNote = new ReleaseNote($data['title'], $data['description'], $data['dateAdded']);

        self::assertReleaseNoteEntity($releaseNote, $data);

        $releaseNote
            ->setTitle($data['title'])
            ->setDescription($data['description'])
            ->setDateAdded($data['dateAdded']);

        self::assertReleaseNoteEntity($releaseNote, $data);
    }

    /**
     * @param ReleaseNote $releaseNote
     * @param array       $expected
     */
    private function assertReleaseNoteEntity(ReleaseNote $releaseNote, array $expected)
    {
        self::assertNull($releaseNote->getId());
        self::assertEquals($expected['title'], $releaseNote->getTitle());
        self::assertEquals($expected['description'], $releaseNote->getDescription());
        self::assertEquals($expected['dateAdded'], $releaseNote->getDateAdded());
        self::assertTrue($releaseNote->getDateAdded() instanceof \DateTime);
    }
}
