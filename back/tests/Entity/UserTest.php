<?php

namespace Tests\Entity;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class UserTest extends TestCase
{
    public function testEntity()
    {
        $data = [
            'login' => 'login',
            'password' => 'passwordHash',
            'email' => 'email@fakemail.com',
            'first' => 'first',
            'last' => 'last',
            'photo' => '/photo.png',
        ];

        $user = new User($data['login'], $data['email'], $data['password'], $data['first'], $data['last'], $data['photo']);

        self::assertUserEntity($user, $data);

        $user
            ->setUsername($data['login'])
            ->setPassword($data['password'])
            ->setEmail($data['email'])
            ->setFirst($data['first'])
            ->setLast($data['last'])
            ->setPhoto($data['photo']);

        self::assertUserEntity($user, $data);

        $user->eraseCredentials();

        self::assertTrue(\is_string($user->getSalt()) || null === $user->getSalt());
    }

    /**
     * @param User  $user
     * @param array $expected
     */
    private function assertUserEntity(User $user, array $expected)
    {
        self::assertEquals($expected['login'], $user->getUsername());
        self::assertEquals($expected['password'], $user->getPassword());
        self::assertEquals($expected['email'], $user->getEmail());
        self::assertEquals($expected['first'], $user->getFirst());
        self::assertEquals($expected['last'], $user->getLast());
        self::assertEquals($expected['photo'], $user->getPhoto());
        self::assertEquals($user->getId().$user->getDateAdded()->getTimestamp(), $user->getSalt());
        self::assertTrue($user->getDateAdded() instanceof \DateTime);
        self::assertIsArray($user->getRoles());
    }
}
