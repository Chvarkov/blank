<?php

namespace App\Component\Exception;

/**
 * Class InternalServerErrorException.
 */
class InternalServerErrorException extends ApiHttpException
{
    protected $code = 500;
}
