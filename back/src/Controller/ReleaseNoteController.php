<?php

namespace App\Controller;

use App\Component\Exception\ApiHttpException;
use App\Component\Exception\BadRequestException;
use App\Component\Exception\InternalServerErrorException;
use App\Component\Exception\NotFoundException;
use App\Component\JsonResponse;
use App\Component\Pagination;
use App\Entity\ReleaseNote;
use App\Form\Type\PaginationType;
use App\Form\Type\ReleaseNoteType;
use App\Repository\ReleaseNoteRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ReleaseNoteController. Resource of @see ReleaseNote.
 *
 * @Rest\Version({"v1"})
 *
 * @Route("/api/release-note", name="api_release_note")
 */
class ReleaseNoteController extends AbstractController
{
    /**
     * Get list of release notes.
     *
     * @Route("", name="_list", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"list"})
     *
     * @param Request               $request
     * @param ReleaseNoteRepository $releaseNoteRepo
     *
     * @return View
     */
    public function getList(Request $request, ReleaseNoteRepository $releaseNoteRepo): View
    {
        try {
            $form = $this
                ->createForm(PaginationType::class, new Pagination(), ['method' => Request::METHOD_GET])
                ->handleRequest($request);

            $pagination = $form->getData();

            if ($form->isSubmitted() && !$form->isValid() || !$pagination instanceof Pagination) {
                throw new BadRequestException($form);
            }

            $list = $releaseNoteRepo->getList($pagination->getLimit(), $pagination->getOffset());

            return (new JsonResponse($list))->getView();
        } catch (\Exception $ex) {
            return $ex instanceof ApiHttpException ? $ex->getView() : (new InternalServerErrorException($ex->getMessage()))->getView();
        }
    }

    /**
     * Add release note.
     *
     * @Route("", name="_add", methods={"POST"})
     *
     * @param Request                $request
     * @param EntityManagerInterface $em
     *
     * @return View
     */
    public function add(Request $request, EntityManagerInterface $em): View
    {
        try {
            $form = $this
                ->createForm(ReleaseNoteType::class, null, ['method' => Request::METHOD_POST])
                ->handleRequest($request);

            $releaseNote = $form->getData();

            if (!$form->isSubmitted() || !$form->isValid() || !$releaseNote instanceof ReleaseNote) {
                throw new BadRequestException($form);
            }

            $em->persist($releaseNote);
            $em->flush();

            return (new JsonResponse())->setCode(204)->getView();
        } catch (\Exception $ex) {
            return $ex instanceof ApiHttpException ? $ex->getView() : (new InternalServerErrorException($ex->getMessage()))->getView();
        }
    }

    /**
     * Edit release note.
     *
     * @Route("/{id}", name="_edit", requirements={"id": "\d+"},  methods={"PUT"})
     *
     * @param int                    $id
     * @param Request                $request
     * @param EntityManagerInterface $em
     *
     * @return View
     */
    public function edit(int $id, Request $request, EntityManagerInterface $em): View
    {
        try {
            $releaseNoteRepo = $em->getRepository(ReleaseNote::class);
            $releaseNote = $releaseNoteRepo->findOneBy(['id' => $id]);

            if (!$releaseNote instanceof ReleaseNote) {
                throw new NotFoundException();
            }

            $form = $this
                ->createForm(ReleaseNoteType::class, $releaseNote, ['method' => Request::METHOD_PUT])
                ->handleRequest($request);

            $releaseNote = $form->getData();

            if (!$form->isSubmitted() || !$form->isValid() || !$releaseNote instanceof ReleaseNote) {
                throw new BadRequestException($form);
            }

            $em->flush();

            return (new JsonResponse())->setCode(204)->getView();
        } catch (\Exception $ex) {
            return $ex instanceof ApiHttpException ? $ex->getView() : (new InternalServerErrorException($ex->getMessage()))->getView();
        }
    }
}
