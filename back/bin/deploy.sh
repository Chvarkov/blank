#!/usr/bin/env bash

ENV=${1}

if [[ ${ENV} != "test" && ${ENV} != "dev" && ${ENV} != "prod" ]]
then
    ENV="dev"
fi

composer install --optimize-autoloader --no-interaction

if [[ "${ENV}" == "test" ]]
then
    php bin/console doctrine:database:drop --if-exists --force -n --env=${ENV}
fi

php bin/console doctrine:database:create --if-not-exists --env=${ENV}
php bin/console doctrine:migrations:migrate --env=${ENV}
php bin/console doctrine:fixtures:load --append --env=${ENV}

php bin/console cache:warmup --env=${ENV}

