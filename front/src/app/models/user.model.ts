export class UserModel {
  public id: number;
  public first: string;
  public last: string;
  public username: string;
  public email: string;
  public photo: string;
  public roles: Array<string> = [];

  public hasRole(role: string): boolean {
    return this.roles.includes(role);
  }
}
