<?php

namespace App\Tests\Form\Type\TestType;

use App\Form\Type\EntityFormType;
use App\Tests\Form\DTO\TestClass;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TestClassFormType.
 */
class TestClassFormType extends EntityFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('bool', CheckboxType::class)
            ->add('int', IntegerType::class)
            ->add('float', IntegerType::class)
            ->add('string', TextType::class)
            ->add('array', CollectionType::class)
            ->add('datetime', DateTimeType::class)
            ->add('nullable', TextType::class)
            ->add('childClass', TestChildrenClassFormType::class)
            ->add('defaultValue', TextType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => TestClass::class,
        ]);
    }
}
