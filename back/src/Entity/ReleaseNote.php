<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class entity of ReleaseNote.
 *
 * @ORM\Table(name="ReleaseNotes")
 * @ORM\Entity(repositoryClass="App\Repository\ReleaseNoteRepository")
 */
class ReleaseNote
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer", length=255, nullable=false)
     *
     * @var int|null
     */
    private $id;

    /**
     * @Assert\NotNull(message="Title cannot be null")
     * @Assert\NotBlank(message="Title cannot be blank")
     * @Assert\Length(min="2",  minMessage="Title cannot be less than 2 characters",
     *                max="255", maxMessage="Title cannot be longer than 255 characters"
     * )
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $title;

    /**
     * @Assert\NotNull(message="Description cannot be null")
     * @Assert\NotBlank(message="Description cannot be blank")
     * @Assert\Length(min="2",  minMessage="Description cannot be less than 2 characters",
     *                max="65535", maxMessage="Description cannot be longer than 65535 characters"
     * )
     *
     * @ORM\Column(name="description", type="string", nullable=false)
     *
     * @var string
     */
    public $description;

    /**
     * @ORM\Column(name="dateAdded", type="datetime", nullable=false)
     *
     * @var \DateTime
     */
    public $dateAdded;

    /**
     * ReleaseNote constructor.
     *
     * @param string         $title
     * @param string         $description
     * @param \DateTime|null $dateAdded
     */
    public function __construct(
        string $title,
        string $description,
        ?\DateTime $dateAdded = null
    ) {
        $this->title = $title;
        $this->description = $description;
        $this->dateAdded = $dateAdded instanceof \DateTime ? $dateAdded : new \DateTime();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return ReleaseNote
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return ReleaseNote
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateAdded(): \DateTime
    {
        return $this->dateAdded;
    }

    /**
     * @param \DateTime $dateAdded
     *
     * @return ReleaseNote
     */
    public function setDateAdded(\DateTime $dateAdded): self
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }
}
