export class ReleaseNoteModel {
  title: string;
  description: string;
  dateAdded: string;
}
