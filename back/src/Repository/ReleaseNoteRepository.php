<?php

namespace App\Repository;

use App\Entity\ReleaseNote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class UserRepository entity of @see ReleaseNote
 * Table in database ReleaseNoteAdd.
 */
class ReleaseNoteRepository extends ServiceEntityRepository
{
    /**
     * ReleaseNoteRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReleaseNote::class);
    }

    /**
     * @param int $limit
     * @param int $offset
     *
     * @return array
     */
    public function getList(int $limit = 15, int $offset = 0): array
    {
        return $this->createQueryBuilder('note')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}
