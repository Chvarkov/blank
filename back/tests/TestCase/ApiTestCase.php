<?php

namespace App\Tests\TestCase;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

class ApiTestCase extends WebTestCase
{
    /** @var Client */
    protected $client;

    /** @var RouterInterface */
    protected $router;

    /** @var array */
    protected $files = [];

    /** @var array */
    protected $body = [];

    /** @var array */
    protected $content = [];

    /** @var string */
    protected $token = '';

    /**
     * {@inheritdoc}
     *
     * @throws \Exception
     */
    protected function setUp()
    {
        $this->client = static::createClient();
        $this->router = self::$container->get('router');

        if (!$this->router instanceof RouterInterface) {
            throw new \Exception('Router not found');
        }
    }

    /**
     * Get URL by route.
     *
     * @param string $route
     * @param array  $parameters
     *
     * @return string
     */
    public function getUrl(string $route, array $parameters = []): string
    {
        return $this->router->generate($route, $parameters);
    }

    /**
     * @param string $url
     * @param string $method
     *
     * @return Response
     */
    public function makeRequest(string $url, string $method = 'GET'): Response
    {
        $this->client->request($method, $url, $this->body, $this->files, [
            'Content-Type' => 'application/json',
            'HTTP_AUTHORIZATION' => 'Bearer '.$this->token,
        ], \json_encode($this->content));

        $this->body = [];
        $this->files = [];
        $this->content = [];

        return $this->client->getResponse();
    }

    /**
     * @param Response $response
     * @param int      $statusCode
     */
    public function assertStatusCode(Response $response, int $statusCode = 200)
    {
        self::assertEquals($statusCode, $response->getStatusCode());
    }

    /**
     * @param $value
     */
    public function assertIsStringNotEmpty($value)
    {
        self::assertIsString($value);
        self::assertTrue(\mb_strlen($value) > 0);
    }

    /**
     * @throws \Exception
     */
    public function authAsTestUser()
    {
        $this->setContent([
            'username' => 'edward',
            'password' => 'edward',
        ]);

        $response = $this->makeRequest($this->getUrl('api_user_auth'), 'POST');
        $content = \json_decode($response->getContent(), true);

        if (!\array_key_exists('token', $content) || !is_string($content['token']) || 0 === \mb_strlen($content['token'])) {
            throw new \Exception('Failed to get token');
        }

        $this->token = $content['token'];
    }

    /**
     * @param array $files
     *
     * @return ApiTestCase
     */
    public function setFiles(array $files): self
    {
        $this->files = $files;

        return $this;
    }

    /**
     * @param array $body
     *
     * @return ApiTestCase
     */
    public function setBody(array $body): self
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @param array $content
     *
     * @return ApiTestCase
     */
    public function setContent(array $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @param string $token
     *
     * @return ApiTestCase
     */
    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }
}
