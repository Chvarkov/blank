<?php

namespace App\Component;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Pagination.
 */
class Pagination
{
    public const DEFAULT_PAGE = 1;
    public const DEFAULT_LIMIT = 15;

    /**
     * @Assert\Type(type="integer", message="Page must be integer")
     * @Assert\Range(min="1", minMessage="Page must be 1 and more")
     *
     * @var int
     */
    private $page = self::DEFAULT_PAGE;

    /**
     * @Assert\Type(type="integer", message="Limit must be integer")
     * @Assert\Range(min="1", max="500", minMessage="Limit must be 1 and more", minMessage="Limit must be low 500")
     *
     * @var int
     */
    private $limit = self::DEFAULT_LIMIT;

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     *
     * @return Pagination
     */
    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     *
     * @return Pagination
     */
    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return ($this->page - 1) * $this->limit;
    }
}
