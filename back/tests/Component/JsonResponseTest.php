<?php

namespace Tests\Component;

use App\Component\JsonResponse;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Symfony\Component\HttpFoundation\Response;

class JsonResponseTest extends TestCase
{
    public function testEmpty()
    {
        $data = $this->getViewAsArray((new JsonResponse())->getView());

        self::assertArrayHasKey('code', $data);
        self::assertEquals(Response::HTTP_OK, $data['code']);

        self::assertArrayHasKey('content', $data);
        self::assertEquals(null, $data['content']);
    }

    public function testOnlyStatusCode()
    {
        $data = $this->getViewAsArray((new JsonResponse())->setCode(Response::HTTP_BAD_REQUEST)->getView());

        self::assertArrayHasKey('code', $data);
        self::assertEquals(Response::HTTP_BAD_REQUEST, $data['code']);

        self::assertArrayHasKey('errors', $data);
        self::assertEquals(Response::$statusTexts[Response::HTTP_BAD_REQUEST], $data['errors']);
    }

    public function testMessageAndStatusCode()
    {
        $errorMessage = 'Test error message';

        $data = $this->getViewAsArray((new JsonResponse($errorMessage))->setCode(Response::HTTP_BAD_REQUEST)->getView());

        self::assertArrayHasKey('code', $data);
        self::assertEquals(Response::HTTP_BAD_REQUEST, $data['code']);

        self::assertArrayHasKey('errors', $data);
        self::assertEquals($errorMessage, $data['errors']);
    }

    public function testContentAndTotal()
    {
        $items = [
            ['id' => 1, 'value' => 'item 1'],
            ['id' => 2, 'value' => 'item 2'],
        ];

        $total = \count($items);

        $data = $this->getViewAsArray((new JsonResponse($items))->setTotal($total)->getView());

        self::assertArrayHasKey('code', $data);
        self::assertEquals(Response::HTTP_OK, $data['code']);

        self::assertArrayHasKey('total', $data);
        self::assertEquals($total, $data['total']);

        self::assertArrayHasKey('content', $data);
        self::assertEquals($items, $data['content']);
    }

    /**
     * @param View $view
     *
     * @return array
     */
    private function getViewAsArray(View $view): array
    {
        $content = $view->getData();

        self::assertTrue(\is_array($content) || null === $content);

        return $content ?? [];
    }
}
