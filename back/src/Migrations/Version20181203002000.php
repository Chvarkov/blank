<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add table ReleaseNote.
 */
class Version20181203002000 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("create table {$schema->getName()}.ReleaseNotes
        (
          id          int auto_increment  primary key,
          title       varchar(255) not null,
          description text         not null,
          dateAdded   datetime     not null
        ) engine=InnoDB default charset=utf8 collate=utf8_unicode_ci");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("drop table {$schema->getName()}.ReleaseNotes");
    }
}
