<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class EntityFormType.
 */
class EntityFormType extends AbstractType implements DataMapperInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setDataMapper($this);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();

            if (!\is_array($data)) {
                return null;
            }

            $form = $event->getForm();
            $class = (string) $form->getConfig()->getDataClass();
            $entity = $form->getData();

            if (!\is_object($entity) || $class !== \get_class($entity)) {
                $entity = $this->getEmptyInstance($class);
            }

            $refObject = new \ReflectionObject($entity);

            foreach ($refObject->getProperties() as $property) {
                $name = $property->getName();

                if (!\array_key_exists($name, $data)) {
                    continue;
                }

                $property->setAccessible(true);
                $property->setValue($entity, $data[$name]);
            }

            $form->setData($entity);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'allow_extra_fields' => true,
            'empty_data' => function (Options $options) {
                $class = $options['data_class'];

                if (null !== $class) {
                    return function (FormInterface $form) use ($class) {
                        if ($form->isEmpty() && !$form->isRequired() || !\class_exists($class)) {
                            return null;
                        }

                        return $this->getEmptyInstance($class);
                    };
                }

                return function (FormInterface $form) {
                    return $form->getConfig()->getCompound() ? [] : '';
                };
            },
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function mapDataToForms($data, $form)
    {
        if (!$form instanceof FormInterface) {
            return;
        }
        /** @var FormInterface $form */
        $class = (string) $form->getConfig()->getDataClass();

        if (!is_object($data) || $class !== \get_class($data)) {
            throw new UnexpectedTypeException($data, $class);
        }

        $refObject = new \ReflectionObject($data);
        $properties = $refObject->getProperties();
        $form = iterator_to_array($form);

        foreach ($properties as $property) {
            $property->setAccessible(true);
            $name = $property->getName();

            if (\array_key_exists($name, $form)) {
                $form[$name]->setData($property->getValue($data));
            }
        }
    }

    /**
     * {@inheritdoc}
     *
     * @throws \ReflectionException
     */
    public function mapFormsToData($form, &$data)
    {
        if (!$form instanceof FormInterface) {
            return;
        }

        /** @var FormInterface[] $formData */
        $formData = \iterator_to_array($form);
        $class = (string) $form->getConfig()->getDataClass();

        if (!\class_exists($class) || !\is_array($formData)) {
            return;
        }

        $refClass = new \ReflectionClass($class);
        $constructor = $refClass->getConstructor();
        $parameters = $constructor instanceof \ReflectionMethod ? $constructor->getParameters() : [];

        $args = [];
        foreach ($parameters as $parameter) {
            $name = $parameter->getName();
            $type = $parameter->getType();

            if (null === $type || !\array_key_exists($name, $formData) && $type->allowsNull()) {
                $args[] = null;
                continue;
            }

            if (!\array_key_exists($name, $formData)) {
                $args[] = $parameter->getDefaultValue();
                continue;
            }

            $value = $formData[$name]->getData();

            if ($this->isTypeOf($value, $type)) {
                $args[] = $value;
                continue;
            }
        }

        $data = $refClass->newInstanceArgs($args);
    }

    /**
     * @param mixed       $value
     * @param string|null $type
     *
     * @return bool
     */
    private function isTypeOf($value, ?string $type)
    {
        switch ($type) {
            case 'bool':      return \is_bool($value);
            case 'int':       return \is_int($value);
            case 'float':     return \is_double($value);
            case 'string':    return \is_string($value);
            case 'array':     return \is_array($value);
            default:          return \class_exists((string) $type) ? $type === \get_class($value) : false;
        }
    }

    /**
     * @param string|null $type
     *
     * @return array|bool|\DateTime|int|object|string|null
     *
     * @throws \ReflectionException
     */
    private function getEmptyValue(?string $type)
    {
        $type = (string) $type;
        switch ($type) {
            case 'bool':      return false;
            case 'int':
            case 'float':     return 0;
            case 'string':    return '';
            case 'array':     return [];
            case 'DateTime':  return new \DateTime();
            default:          return \class_exists($type) ? $this->getEmptyInstance($type) : null;
        }
    }

    /**
     * @param string $class
     *
     * @return object
     *
     * @throws \ReflectionException
     */
    private function getEmptyInstance(string $class)
    {
        $refClass = new \ReflectionClass($class);
        $constructor = $refClass->getConstructor();
        $parameters = $constructor instanceof \ReflectionMethod ? $constructor->getParameters() : [];

        // Collect arguments
        $args = [];
        foreach ($parameters as $parameter) {
            $type = $parameter->getType();

            if (null === $type || $type->allowsNull()) {
                $args[] = null;
                continue;
            }

            $args[] = $this->getEmptyValue($type);
        }

        return $refClass->newInstanceArgs($args);
    }
}
