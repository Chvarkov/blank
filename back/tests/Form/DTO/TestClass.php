<?php

namespace App\Tests\Form\DTO;

use Symfony\Bridge\Doctrine\Tests\Form\Type\EntityTypeTest;

/**
 * Test class TestClass for @see EntityTypeTest.
 */
class TestClass
{
    /** @var bool */
    private $bool;

    /** @var int */
    private $int;

    /** @var float */
    private $float;

    /** @var string */
    private $string;

    /** @var array */
    private $array;

    /** @var string|null */
    private $nullable;

    /** @var \DateTime */
    private $datetime;

    /** @var string */
    private $defaultValue;

    /** @var mixed */
    private $mixedType;

    /** @var string|null */
    private $unknownForForm;

    /** @var TestChildrenClass */
    private $childClass;

    /**
     * TestClass constructor.
     *
     * @param bool        $bool
     * @param int         $int
     * @param float       $float
     * @param string      $string
     * @param array       $array
     * @param string|null $nullable
     * @param \DateTime   $datetime
     * @param $mixedType
     * @param string|null       $unknownForForm
     * @param TestChildrenClass $childClass
     * @param string            $defaultValue
     */
    public function __construct(
        bool $bool,
        int $int,
        float $float,
        string $string,
        array $array,
        ?string $nullable,
        \DateTime $datetime,
        $mixedType,
        ?string $unknownForForm,
        TestChildrenClass $childClass,
        string $defaultValue = 'Test'
    ) {
        $this->bool = $bool;
        $this->int = $int;
        $this->float = $float;
        $this->string = $string;
        $this->array = $array;
        $this->nullable = $nullable;
        $this->datetime = $datetime;
        $this->mixedType = $mixedType;
        $this->unknownForForm = $unknownForForm;
        $this->childClass = $childClass;
        $this->defaultValue = $defaultValue;
    }

    /**
     * @return bool
     */
    public function isBool(): bool
    {
        return $this->bool;
    }

    /**
     * @return int
     */
    public function getInt(): int
    {
        return $this->int;
    }

    /**
     * @return float
     */
    public function getFloat(): float
    {
        return $this->float;
    }

    /**
     * @return string
     */
    public function getString(): string
    {
        return $this->string;
    }

    /**
     * @return array
     */
    public function getArray(): array
    {
        return $this->array;
    }

    /**
     * @return string|null
     */
    public function getNullable(): ?string
    {
        return $this->nullable;
    }

    /**
     * @return \DateTime
     */
    public function getDatetime(): \DateTime
    {
        return $this->datetime;
    }

    /**
     * @return string
     */
    public function getDefaultValue(): string
    {
        return $this->defaultValue;
    }

    /**
     * @return TestChildrenClass
     */
    public function getChildClass(): TestChildrenClass
    {
        return $this->childClass;
    }
}
