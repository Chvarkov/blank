import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
export class ReleaseNoteService {

  private apiHost = 'http://localhost:8000';

  private baseUri = '/api/release-note';

  public constructor(private http: HttpClient) {}

  public getList(): Promise<any> {

    const httpHeaders: HttpHeaders = (new HttpHeaders())
      .append('Content-type', 'application/json; charset=utf-8')
      .append('authorization', 'Bearer ' + localStorage.getItem('token_access'));

    return this.http.get(this.apiHost + this.baseUri, { headers: httpHeaders }).toPromise();
  }
}
