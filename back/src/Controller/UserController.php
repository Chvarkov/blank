<?php

namespace App\Controller;

use App\Component\Exception\ApiHttpException;
use App\Component\Exception\BadRequestException;
use App\Component\Exception\InternalServerErrorException;
use App\Component\JsonResponse;
use App\Component\UserRegister;
use App\Entity\User;
use App\Form\Type\UserRegisterType;
use App\Form\Type\UserType;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Class UserController.
 *
 * @Rest\Version({"v1"})
 *
 * @Route("/api/user", name="api_user")
 */
class UserController extends AbstractController
{
    /**
     * Register user.
     *
     * @Route("/register", name="_register", methods={"POST"})
     *
     * @param Request                      $request
     * @param EntityManagerInterface       $em
     * @param UserPasswordEncoderInterface $passwordEncoder
     *
     * @return View
     */
    public function register(
        Request $request,
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $passwordEncoder
    ): View {
        try {
            $form = $this
                ->createForm(UserRegisterType::class, null, ['method' => Request::METHOD_POST])
                ->handleRequest($request);

            $register = $form->getData();

            if (!$form->isSubmitted() || !$form->isValid() || !$register instanceof UserRegister) {
                throw new BadRequestException($form);
            }

            if (!$register->isEqualPasswords()) {
                throw new BadRequestException('Passwords must be equals');
            }

            $user = $register
                ->encodePassword($passwordEncoder)
                ->getUser();

            $em->persist($user);
            $em->flush();

            return (new JsonResponse(sprintf('User %s successfully created', $user->getUsername())))->getView();
        } catch (\Exception $ex) {
            if ($ex instanceof UniqueConstraintViolationException) {
                if (\is_int(strpos($ex->getMessage(), 'Users_email_uIndex'))) {
                    return (new JsonResponse('User with this email already exists', Response::HTTP_BAD_REQUEST))->getView();
                }
                if (\is_int(strpos($ex->getMessage(), 'Users_username_uIndex'))) {
                    return (new JsonResponse('User with this username already exists', Response::HTTP_BAD_REQUEST))->getView();
                }
            }

            return $ex instanceof ApiHttpException ? $ex->getView() : (new InternalServerErrorException($ex->getMessage()))->getView();
        }
    }

    /**
     * Edit user.
     *
     * @Route("", name="_edit", methods={"PUT"})
     *
     * @param Request                      $request
     * @param Security                     $security
     * @param EntityManagerInterface       $em
     * @param UserPasswordEncoderInterface $passwordEncoder
     *
     * @return View
     */
    public function edit(
        Request $request,
        Security $security,
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $passwordEncoder
    ): View {
        try {
            $user = $security->getUser();

            $form = $this
                ->createForm(UserType::class, $user, ['method' => Request::METHOD_PUT])
                ->handleRequest($request);

            $user = $form->getData();

            if (!$form->isSubmitted() || !$form->isValid() || !$user instanceof User) {
                throw new BadRequestException($form);
            }

            $user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()));

            $em->persist($user);
            $em->flush();

            return (new JsonResponse())->setCode(Response::HTTP_NO_CONTENT)->getView();
        } catch (\Exception $ex) {
            return $ex instanceof ApiHttpException ? $ex->getView() : (new InternalServerErrorException($ex->getMessage()))->getView();
        }
    }
}
