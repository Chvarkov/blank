<?php

namespace App\Tests\Form\DTO;

class TestChildrenClass
{
    /** @var bool */
    private $bool;

    /** @var int */
    private $int;

    /** @var float */
    private $float;

    /** @var string */
    private $string;

    /** @var array */
    private $array;

    /** @var string|null */
    private $nullable;

    /** @var \DateTime */
    private $datetime;

    /** @var string */
    private $defaultValue;

    /**
     * TestChildrenClass constructor.
     *
     * @param bool        $bool
     * @param int         $int
     * @param float       $float
     * @param string      $string
     * @param array       $array
     * @param string|null $nullable
     * @param \DateTime   $datetime
     * @param string      $defaultValue
     */
    public function __construct(
        bool $bool,
        int $int,
        float $float,
        string $string,
        array $array,
        ?string $nullable,
        \DateTime $datetime,
        string $defaultValue = 'Test'
    ) {
        $this->bool = $bool;
        $this->int = $int;
        $this->float = $float;
        $this->string = $string;
        $this->array = $array;
        $this->nullable = $nullable;
        $this->datetime = $datetime;
        $this->defaultValue = $defaultValue;
    }

    /**
     * @return bool
     */
    public function isBool(): bool
    {
        return $this->bool;
    }

    /**
     * @return int
     */
    public function getInt(): int
    {
        return $this->int;
    }

    /**
     * @return float
     */
    public function getFloat(): float
    {
        return $this->float;
    }

    /**
     * @return string
     */
    public function getString(): string
    {
        return $this->string;
    }

    /**
     * @return array
     */
    public function getArray(): array
    {
        return $this->array;
    }

    /**
     * @return string|null
     */
    public function getNullable(): ?string
    {
        return $this->nullable;
    }

    /**
     * @return \DateTime
     */
    public function getDatetime(): \DateTime
    {
        return $this->datetime;
    }

    /**
     * @return string
     */
    public function getDefaultValue(): string
    {
        return $this->defaultValue;
    }
}
