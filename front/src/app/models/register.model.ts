export class RegisterModel {
  first: string;
  last: string;
  username: string;
  email: string;
  password: string;
  repeatPassword: string;
}
