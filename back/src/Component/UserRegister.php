<?php

namespace App\Component;

use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UserRegister.
 */
class UserRegister extends User
{
    /**
     * @Assert\NotNull(message="Repeat password cannot be null")
     * @Assert\NotBlank(message="Repeat password cannot be blank")
     * @Assert\Length(min="6",  minMessage="Password cannot be less than 6 characters",
     *                max="32", maxMessage="Password cannot be longer than 32 characters"
     * )
     *
     * @var string
     */
    private $repeatPassword;

    /**
     * UserRegister constructor.
     *
     * @param string         $login
     * @param string         $email
     * @param string         $password
     * @param string         $repeatPassword
     * @param string         $first
     * @param string         $last
     * @param string|null    $photo
     * @param \DateTime|null $dateAdded
     */
    public function __construct(
        string $login,
        string $email,
        string $password,
        string $repeatPassword,
        string $first,
        string $last,
        ?string $photo = null,
        ?\DateTime $dateAdded = null
    ) {
        $this->repeatPassword = $repeatPassword;

        parent::__construct($login, $email, $password, $first, $last, $photo, $dateAdded);
    }

    /**
     * @return bool
     */
    public function isEqualPasswords(): bool
    {
        return $this->getPassword() === $this->repeatPassword;
    }

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     *
     * @return UserRegister
     */
    public function encodePassword(UserPasswordEncoderInterface $passwordEncoder): self
    {
        $this->password = $passwordEncoder->encodePassword($this, $this->password);
        $this->repeatPassword = $this->password;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return new User(
            $this->username,
            $this->email,
            $this->password,
            $this->first,
            $this->last,
            $this->photo,
            $this->dateAdded
        );
    }
}
