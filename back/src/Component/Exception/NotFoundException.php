<?php

namespace App\Component\Exception;

/**
 * Class NotFoundException.
 */
class NotFoundException extends ApiHttpException
{
    protected $code = 404;
}
